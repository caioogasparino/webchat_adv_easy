import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    deleteUser
} from "../redux/actions/login";
import {bindActionCreators} from "redux";

import logo from '../assets/adveasy_fundoescuro.png';

class Header extends React.Component {

    constructor(props){
        super(props);
    }

    doLogout = () => {
        this.props.deleteUser();
    }

    render() {
        return (
            <div className="Header">
                <div className="HeaderContent">
                    <img src={logo} alt="logo" />
                    <div className="HeaderActions">
                        <button className="MenuItem">Ajuda</button>
                        <button className="MenuItem" onClick={() => this.doLogout()}>Sair</button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
    deleteUser
}, dispatch);
export default connect(null, mapDispatchToProps)(Header);