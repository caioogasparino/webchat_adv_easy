import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    deleteUser
} from "../../redux/actions/login";
import {bindActionCreators} from "redux";

import Avatar from '../Avatar';
import UserCard from '../UserCard';
import {
    MdClearAll,
    MdExitToApp,
    MdNotificationsActive,
    MdNotificationsNone,
} from 'react-icons/md';
import {
    Button,
    ListGroup,
    ListGroupItem,
    // NavbarToggler,
    Nav,
    Navbar,
    NavItem,
    NavLink,
    Popover,
    PopoverBody,
} from 'reactstrap';

import logo from '../../assets/adveasy_fundoescuro.png';

class Header extends React.Component {
    constructor(props){
        super(props);
    }

    state = {
        isOpenUserCardPopover: false,
    };
    
    toggleUserCardPopover = () => {
        this.setState({
            isOpenUserCardPopover: !this.state.isOpenUserCardPopover,
        });
    };

    doLogout = () => {
        this.props.deleteUser();
    }

    render() {
        return (
            <div className="Header">
                <div className="HeaderContent">
                    <img src={logo} alt="logo" />
                    <div className="HeaderActions">
                        <Nav>
                        <NavItem>
                            <NavLink id="Popover2">
                                <Avatar
                                    onClick={this.toggleUserCardPopover}
                                    className="can-click"
                                />
                                </NavLink>
                                <Popover
                                placement="bottom-end"
                                isOpen={this.state.isOpenUserCardPopover}
                                toggle={this.toggleUserCardPopover}
                                target="Popover2"
                                className="p-0 border-0"
                                style={{ minWidth: 250 }}
                                >
                                <PopoverBody className="p-0 border-light">
                                    <UserCard
                                    title="Carlos Alberto"
                                    subtitle="Advogado"
                                    className="border-light"
                                    >
                                    <ListGroup flush>
                                        <ListGroupItem tag="button" action className="border-light" onClick={() => this.doLogout()}>
                                            <MdExitToApp /> Sair
                                        </ListGroupItem>
                                    </ListGroup>
                                    </UserCard>
                                </PopoverBody>
                                </Popover>
                            </NavItem>
                            </Nav>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
    deleteUser
}, dispatch);
export default connect(null, mapDispatchToProps)(Header);