import React from 'react';
import { useSelector } from 'react-redux';

import Login from './Login';
import Chat from './Chat';

function App() {
	const isLoged = useSelector(state => state.setup.userLogged);
	console.log(isLoged);

	if(!isLoged) {
		return (<Login />);
	} else {
		return (<Chat />);
	}
}

export default App;
